/***********************************************************************************************************************
 *** User: class that represents all data that represents a user in the userList
 **********************************************************************************************************************/
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());
var buecher = /** @class */ (function () {
    function buecher() {
    }
    return buecher;
}());
/**********************************************************************************************************************
 *** Class and Object dealing with settings                                                                           *
 **********************************************************************************************************************/
//--- Class handling settings ------------------------------------------------------------------------------------------
var settingsClass = /** @class */ (function () {
    function settingsClass(options) {
        this.options = options; // just copy given array into internal array
    }
    settingsClass.prototype.storeSettings = function () {
        for (var index in this.options) {
            localStorage.setItem("option" + index, this.options[index] == true ? "true" : "false");
        }
    };
    settingsClass.prototype.restoreSettings = function () {
        for (var index in this.options) {
            this.options[index] = (localStorage.getItem("option" + index) == "true");
        }
    };
    return settingsClass;
}());
//--- Global variable to store settings --------------------------------------------------------------------------------
var settings = new settingsClass([false, true, true]); // initialize with three settings
/**********************************************************************************************************************
 *** Event-handlers                                                                                                   *
 ***  1. Handler, when clicking the "+"-button or pressing "return" in user-input-field (CREATE)                      *
 ***  2. Handler, when clicking the trash-icon (DELETE)                                                               *
 ***  3. Handler, when clicking on an element in user-line: open edit modal window (READ)                             *
 ***  4. Handler, when clicking on "save"-button (in edit modal window): Save detail information of a user (UPDATE)   *
 ***  5. Handler, when clicking on "cancel"-button (edit window): close edit window                                   *
 ***  6. Handler, when typing filter-characters                                                                       *  *
 ***  7. Handler, when clicking on "save"-button (in sessions modal window): Save settings                            *
 ***  8. Handler, when clicking on "cancel"-button (in sessions modal window): reset check-flags and close window     *
 *** 9. Handler, when clicking on "login"-button: login                                                              *
 *** 10. Handler, when clicking on "logout"-button: logout                                                            *
 **********************************************************************************************************************/
//--- 1. Handler, when clicking the "+"-button or pressing "return" in user-input-field (CREATE) -----------------------
function handleCreate() {
    var usernameInput = $('#usernameInput'); // input field of username
    var passwordInput = $('#passwordInput'); // input field of password
    var vornameInput = $('#vornameInput'); // input field of vorname
    var nachnameInput = $('#nachnameInput'); // input field of nachname
    var emailInput = $('#emailInput'); // input field of nachname
    // Prevent empty names
    if ((usernameInput.val().trim().length !== 0) &&
        (passwordInput.val().trim().length !== 0) &&
        (vornameInput.val().trim().length !== 0) &&
        (nachnameInput.val().trim().length !== 0) &&
        (emailInput.val().trim().length !== 0)) {
        var data = { "username": usernameInput.val(),
            "password": passwordInput.val(),
            "vorname": vornameInput.val(),
            "nachname": nachnameInput.val(),
            "email": emailInput.val()
        };
        $.ajax({
            url: 'http://localhost:8080/user',
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            error: function (jqXHR) {
                alert("handleCreate error");
            },
            success: function (data) {
                renderList(data.userList);
                alert("Sie haben sich erfolgreich registriert!");
            }
        });
        // initialize input field
        usernameInput.val("");
        passwordInput.val("");
        vornameInput.val("");
        nachnameInput.val("");
        emailInput.val("");
    }
}
//--- 2. Handler, when clicking the trash-icon (DELETE) ----------------------------------------------------------------
function handleDelete(event) {
    // the method stopPropagation() stops the bubbling of an event to parent elements,
    // so in this case it prevents "handleRead" from popping up edit window
    event.stopPropagation();
    // delete element with id = userId
    var id = $(this).attr("id"); // "this" referes to current span-element
    $.ajax({
        url: 'http://localhost:8080/user/' + id,
        type: 'DELETE',
        dataType: 'json',
        error: function (jqXHR) { renderResult(jqXHR.responseJSON.message, jqXHR.status); },
        success: function (data) { renderResult(data.message, 0); renderList(data.userList); }
    });
}
function handleDelete2(event) {
    // the method stopPropagation() stops the bubbling of an event to parent elements,
    // so in this case it prevents "handleRead" from popping up edit window
    event.stopPropagation();
    // delete element with id = userId
    var id = $(this).attr("id"); // "this" referes to current span-element
    $.ajax({
        url: 'http://localhost:8080/buecher/' + id,
        type: 'DELETE',
        dataType: 'json',
        error: function (jqXHR) { renderResult(jqXHR.responseJSON.message, jqXHR.status); },
        success: function (data) { renderResult(data.message, 0); renderList2(data.buecherList); }
    });
}
//--- 3. Handler, when clicking on an element in user-line: open edit modal window (READ) ------------------------------
function handleEdit() {
    var id = $(this).attr("id"); // "this" referes to current li element
    $.ajax({
        url: 'http://localhost:8080/user/' + id,
        type: 'GET',
        dataType: 'json',
        error: function (jqXHR) { alert("handleEdit error"); },
        success: function (data) { renderEdit(id, data.user); }
    });
}
//--- 3.1 Handler, when clicking on the "Login"-Button, to get the users rights_id -------------------------------------
function handleGetLogin() {
    var username = $('#username').val();
    $.ajax({
        url: 'http://localhost:8080/user/' + username,
        type: 'GET',
        dataType: 'json',
        error: function (jqXHR) { alert("handleEdit Error"); },
        success: function (data) { handleLogin(data.rights_id); }
    });
}
//--- 4. Handler, when clicking on "save"-button (edit window): Save detail information of a user (UPDATE) -------------
function handleEditSave() {
    var editWindow = $('#editWindow');
    var editPassword = $('#editPassword');
    var id = editWindow.attr("currentUserId");
    // get user and set attributes
    var data = { "vorname": $('#editVorname').val(),
        "nachname": $('#editNachname').val(),
        "password": editPassword.val() };
    $.ajax({
        url: 'http://localhost:8080/user/' + id,
        type: 'PUT',
        data: JSON.stringify(data),
        contentType: 'application/json',
        dataType: 'json',
        error: function (jqXHR) { renderResult(jqXHR.responseJSON.message, jqXHR.status); },
        success: function (data) { renderResult(data.message, 0); renderList(data.userList); }
    });
    editPassword.val(""); // reset initial value of password
    editWindow.css("display", "none"); // Close edit window
}
//--- 5. Handler, when clicking on "cancel"-button (edit window): close edit window ------------------------------------
function handleEditCancel() {
    $('#editWindow').css("display", "none");
}
//--- 6. Handler, when typing filter-characters ------------------------------------------------------------------------
function handleFilter() {
    var searchValue = ($('#userFilter').val());
    $('#userUL').children().each(function () {
        if (($(this).find(".vornameText").text().indexOf(searchValue) >= 0) ||
            ($(this).find(".nachnameText").text().indexOf(searchValue) >= 0)) {
            $(this).removeClass("hide"); // remove class to hide element -> show it
        }
        else {
            $(this).addClass("hide"); // add class to hide element
        }
    });
}
//--- 7. Handler, when clicking on "save"-button (settings window): save check-flags -----------------------------------
function handleSettingsSave() {
    var optionCheck;
    for (var index in settings.options) {
        optionCheck = $("#option" + index + "Check"); // uses twice -> store
        settings.options[index] = (optionCheck.is(":checked"));
        optionCheck.prop("checked", settings.options[index]); // set "checked" to true or false
    }
    $('#settingsWindow').css("display", "none"); // close window
}
//--- 8. Handler, when clicking on "cancel"-button (in sessions modal window): reset check-flags and close window ------
function handleSettingsCancel() {
    for (var index in settings.options) {
        $("#option" + index + "Check").prop("checked", settings.options[index]); // reset "checked" to initial value
    }
    $('#settingsWindow').css("display", "none"); // close window
}
//--- 9. Handler, when clicking on "login"-button: login     --> check login as user or admin(rightsid)//
function handleLogin(rights_id) {
    // Ajax-Request  : POST http://localhost:8080/login
    var data = { username: $('#username').val(), password: $('#password').val() };
    $.ajax({
        url: 'http://localhost:8080/login',
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        dataType: 'json',
        error: function (jqXHR) { alert("error"); /**renderResult(jqXHR.responseJSON.message, jqXHR.status)*/ },
        success: function (data) {
            alert("Sie haben sich erfolgreich eingeloggt!");
            if (rights_id == 2) {
                $(".mitgliederbereich").show();
                $("#profilAdmin, #profilSideAdmin").show();
                $(".login").hide();
                $("#register, #registerSide").hide();
                $("#profil, #profilSide").hide();
                $(".loginadmin").show();
                renderList(data.userList);
                renderList2(data.buecherList);
            }
            else {
                $(".mitgliederbereich").show();
                $("#profil, #profilSide").show();
                $(".login").hide();
                $("#register, #registerSide").hide();
                $(".loginuser").show();
            }
        }
        /**renderResult(data.message, 0);
        renderList(data.userList);
        $("#username").html('user: ' + data.username );
        $("#contentArea").show();  // show content area: filter and userList
        $("#login").hide();        // hide login
        $("#logout").show();       // show logout
        $("#userIn").val("");
        $("#passwordIn").val("");*/
    });
}
//--- 10. Handler, when clicking on "logout"-button: logout ------------------------------------------------------------
function handleLogout() {
    // Ajax-Request  : POST http://localhost:8080/logout
    $.ajax({
        url: 'http://localhost:8080/logout',
        type: 'POST',
        dataType: 'json',
        headers: {},
        error: function (jqXHR) { renderResult(jqXHR.responseJSON.message, jqXHR.status); },
        success: function (data) {
            $(".mitgliederbereich").hide(); // hide content area: filter and userList
            $("#profil, #profilSide, #profilAdmin, #profilSideAdmin").hide(); // hide logout
            $(".login").show(); // show login
            $("#register, #registerSide").show();
        }
    });
}
//--- 10.1 Handler, when clicking on elements to check if the session is still running ---------------------------------
function handleBtn() {
    var buttonID = this.getAttribute("id");
    $.ajax({
        url: 'http://localhost:8080/btn/' + buttonID,
        type: 'POST',
        dataType: 'json',
        headers: {},
        error: function (jqXHR) {
            $(".mitgliederbereich").hide(); // hide content area: filter and userList
            $("#profil, #profilSide, #profilAdmin, #profilSideAdmin").hide(); // hide logout
            $(".login").show(); // show login
            $("#register, #registerSide").show();
            alert("Session expired");
        },
        success: function (data) { }
    });
}
//--- 11. Handler, when clicking on the profil-icon for users ----------------------------------------------------------
function handleProfile() {
    $("#closeProfil").click(function () {
        $("#profilWindow").css("display", "none");
    });
    $("#profilTitle").html(this.username);
    $("#profilWindow").css("display", "block");
}
//--- 12. Handler, when clicking on the profil-icon for admins ---------------------------------------------------------
function handleAdminProfile() {
    $("#closeAdminProfil").click(function () {
        $("#profilAdminWindow").css("display", "none");
    });
    $("#adminTitle").html("Welcome Admin");
    $("#profilAdminWindow").css("display", "block");
}
function handleBuchEinstellen() {
    var data = {
        titel: $('#titel').val(),
        autor: $('#autor').val(),
        genre: $('.currentGenreVal').val(),
        erscheinungsjahr: $('#erscheinungsjahr').val(),
        verlag: $('#verlag').val(),
        sprache: $('#sprache').val(),
        seitenanzahl: $('#seitenanzahl').val(),
        isbn: $('#isbn').val(),
        beschreibung: $('#beschreibung').val()
    };
    $.ajax({
        url: 'http://localhost:8080/buch/anlegen',
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        dataType: 'json',
        error: function (jqXHR) { renderResult(jqXHR.responseJSON.message, jqXHR.status); },
        success: function (data) {
            alert("Vielen Dank! Ihr Inserat wird von uns geprüft und in Kürze veröffentlicht.");
        }
    });
}
//--- 13. function, when sending a rating  ---------------------------------------------------------
function handleBewertungEinstellen() {
    var data = {
        bewertungstext: $('#bewertungsComment').val()
    };
    $.ajax({
        url: 'http://localhost:8080/bewertung/anlegen',
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        dataType: 'json',
        error: function (jqXHR) { renderResult(jqXHR.responseJSON.message, jqXHR.status); },
        success: function (data) {
            alert("success");
        }
    });
}
/**********************************************************************************************************************
 *** Functions that checks the selections in Radio-/Checkboxes
 **********************************************************************************************************************/
function checkGender() {
    if ($("input[name='gender']:checked").length > 0) {
        return $("input[name='gender']:checked").val();
    }
    else {
        return 0;
    }
}
function checkInterest(interest) {
    if ($('#' + interest).is(":checked")) {
        return $('#' + interest).val();
    }
    else {
        return 0;
    }
}
/**********************************************************************************************************************
 *** Function that is called initially to check a user for logging in
 **********************************************************************************************************************/
function checkLogin() {
    $.ajax({
        url: 'http://localhost:8080/login/check',
        type: 'GET',
        dataType: 'json',
        error: function () { },
        success: function (data) {
            $("#username").html('user: ' + data.username);
            $("#contentArea").show(); // show content area, filter and userList
            $("#login").hide(); // hide login
            $("#logout").show(); // show logout
            $("#profil").show(); // show profil-icon
            renderResult(data.message, 0);
            renderList(data.userList);
            renderList2(data.buecherList);
        }
    });
}
/**********************************************************************************************************************
 *** Functions that renders result, userlist or edit-window
 *** 1. render the userlist
 *** 2. render the result (message and error code)
 *** 3. render the contents of the edit-window
 *** 4. render the contents of the settings-window
 **********************************************************************************************************************/
//--- 1. render Lists --------------------------------------------------------------------------------------------------
function renderList(userList) {
    /*
     constructs a list of li-elements with subsequent DOM elements displaying users
     its time/time and an erase-icon and appends this list to ul-element.
     Adds two event-listeners when clicking on some element in line or on the trash-icon
     Example:
     <li id="0">
     <div class="w3-tiny">Mustermann</div>
     <div>
     <span class="userVorname">Max</span>
     <span class="userNachname">Mustermann</span>
     <span class="fa fa-trash w3-large w3-margin-right" id="0"></span>
     </div>
     <div class="w3-tiny">Thu, 12 Jan 2017 13:45:00 GMT</div>
     </li>
     <li> ... </li>
     */
    var userUL = $('#userUL'); // the user list in which the  users will be added
    // remove DOM-subtree below ul-element
    userUL.empty();
    // iterate through all users build li-element with all subelements (see example in methode-header)
    for (var id in userList) {
        var user = userList[id]; // get user
        if (user != null) {
            //--- create and configure li-element ----------------------------------------------------------------------------
            // Example: <li id="0">
            var li = document.createElement('li');
            li.id = String(user.id); // set id as attribute to access in event-handler
            li.addEventListener('click', handleEdit); // register event-handler
            //--- create div-element that wraps following spans --------------------------------------------------------------
            var div = document.createElement('div');
            //--- create and configure div-Element that handles time/time stamp ----------------------------------------------
            // Example:  <div class="w3-tiny">Mustermann</div>
            var divUsername = document.createElement('div');
            divUsername.classList.add("w3-tiny");
            divUsername.textContent = user.username;
            //--- create and configure span-element that handles vorname -----------------------------------------------------
            // Example::     <span class="vornameText">Max</span>
            var spanVorname = document.createElement('span');
            spanVorname.classList.add("vornameText");
            spanVorname.textContent = user.firstname;
            //--- create and configure span-element that handles nachname ----------------------------------------------------
            // Example::     <span class="nachnameText">Mustermann</span>
            var spanNachname = document.createElement('span');
            spanNachname.classList.add("nachnameText");
            spanNachname.textContent = user.lastname;
            //--- create and configure span-Element that handles delete-button -----------------------------------------------
            // Example:      <span class="fa fa-trash w3-large w3-margin-right" id="0"></span>
            var spanDelete = document.createElement('span');
            spanDelete.classList.add('fa', 'fa-trash', "w3-large", "w3-margin-right");
            spanDelete.id = String(user.id); // set id as attribute to access in event-handler
            spanDelete.addEventListener('click', handleDelete); // register event-handler
            //--- create and configure div-Element that handles time/time stamp ----------------------------------------------
            // Example:  <div class="w3-tiny">Thu, 12 Jan 2017 13:45:00 GMT</div>
            var divDateTime = document.createElement('div');
            divDateTime.classList.add("w3-tiny");
            if (user.time) {
                divDateTime.innerText = user.registered;
            }
            //--- build DOM-subtree starting with li and add it to userList --------------------------------------------------
            li.appendChild(divUsername);
            div.appendChild(spanVorname);
            div.appendChild(spanNachname);
            div.appendChild(spanDelete);
            li.appendChild(div);
            li.appendChild(divDateTime);
            userUL.append(li);
        }
    }
}
function renderList2(bookList) {
    /*
     constructs a list of li-elements with subsequent DOM elements displaying users
     its time/time and an erase-icon and appends this list to ul-element.
     Adds two event-listeners when clicking on some element in line or on the trash-icon
     Example:
     <li id="0">
     <div class="w3-tiny">Mustermann</div>
     <div>
     <span class="userVorname">Max</span>
     <span class="userNachname">Mustermann</span>
     <span class="fa fa-trash w3-large w3-margin-right" id="0"></span>
     </div>
     <div class="w3-tiny">Thu, 12 Jan 2017 13:45:00 GMT</div>
     </li>
     <li> ... </li>
     */
    var userUL = $('#buecherUL'); // the user list in which the  users will be added
    // remove DOM-subtree below ul-element
    userUL.empty();
    // iterate through all users build li-element with all subelements (see example in methode-header)
    for (var id in bookList) {
        var buecher_1 = bookList[id]; // get user
        if (buecher_1 != null) {
            //--- create and configure li-element ----------------------------------------------------------------------------
            // Example: <li id="0">
            var li = document.createElement('li');
            li.id = String(buecher_1.id); // set id as attribute to access in event-handler
            li.addEventListener('click', handleEdit); // register event-handler
            //--- create div-element that wraps following spans --------------------------------------------------------------
            var div = document.createElement('div');
            //--- create and configure div-Element that handles time/time stamp ----------------------------------------------
            // Example:  <div class="w3-tiny">Mustermann</div>
            var divUsername = document.createElement('div');
            divUsername.classList.add("w3-tiny");
            divUsername.textContent = buecher_1.Titel;
            //--- create and configure span-element that handles vorname -----------------------------------------------------
            // Example::     <span class="vornameText">Max</span>
            var spanVorname = document.createElement('span');
            spanVorname.classList.add("vornameText");
            spanVorname.textContent = buecher_1.Autor;
            //--- create and configure span-element that handles nachname ----------------------------------------------------
            // Example::     <span class="nachnameText">Mustermann</span>
            //--- create and configure span-Element that handles delete-button -----------------------------------------------
            // Example:      <span class="fa fa-trash w3-large w3-margin-right" id="0"></span>
            var spanDelete = document.createElement('span');
            spanDelete.classList.add('fa', 'fa-trash', "w3-large", "w3-margin-right");
            spanDelete.id = String(buecher_1.id); // set id as attribute to access in event-handler
            spanDelete.addEventListener('click', handleDelete2); // register event-handler
            //--- create and configure div-Element that handles time/time stamp ----------------------------------------------
            // Example:  <div class="w3-tiny">Thu, 12 Jan 2017 13:45:00 GMT</div>
            //--- build DOM-subtree starting with li and add it to userList --------------------------------------------------
            li.appendChild(divUsername);
            div.appendChild(spanVorname);
            div.appendChild(spanDelete);
            li.appendChild(div);
            userUL.append(li);
        }
    }
}
//--- 2. render the result (message and error code) --------------------------------------------------------------------
var resultTimers = []; // array that stores all resultTimers that are set
function hideAfter(seconds, domElement) {
    for (var i in resultTimers) {
        clearTimeout(resultTimers[i]);
    } // iterate through all resultTimers and clear them
    resultTimers.push(setTimeout(function () {
        domElement.css("display", "none"); // hide domElement after given seconds
    }, seconds * 1000)); // set new timer and store it in resultTimers-array
}
function renderResult(text, status) {
    var resultWindow = $("#resultWindow");
    var result = $("#result");
    result.html(text);
    if (status > 0) {
        if (status == 401) {
            $("#contentArea").hide(); // hide content area
            $("#logout").hide(); // hide logout
            $("#login").show(); // show login#
        }
        result.removeClass("w3-teal");
        result.addClass("w3-orange");
    }
    else {
        result.removeClass("w3-orange");
        result.addClass("w3-teal");
    }
    // show only if settings.options[0] (= Expert Mode) == true
    if (settings.options[0]) {
        resultWindow.css("display", "block");
        hideAfter(2, resultWindow); // hide result after three seconds
    }
}
//--- 3. render the contents of the edit-window ------------------------------------------------------------------------
function renderEdit(userID, user) {
    $('#editTitle').text(user.username);
    $('#editVorname').val(user.vorname); // set value of "vorname" field to text of provided vorname
    $('#editNachname').val(user.nachname); // set value of "nachname" field to text of provided nachname
    $('#editDate').val(user.time); // set value of "Date" input-field to time of provided user
    // store Id in attribute of DOM-element and show edit-window
    $('#editWindow').attr("currentUserId", userID).css("display", "block");
}
/***********************************************************************************************************************
 ***
 *** Main Event Listener, that waits until DOM is loaded
 *** 1. (re-)entering application:
 ***    - check, if user is already logged in (e.g. after refresh)
 *** 2. Event Handler
 *** 3. Leaving application
 ***
 **********************************************************************************************************************/
$(function () {
    /** 1. (re-)entering application
    */
    settings.restoreSettings(); // restore the settings from local storage
    checkLogin(); // check, if user is already logged in (e.g. after refresh)
    /**
     * 2. Event Handler
     */
    //--- 1. click on the "+"-button or "return" : Add a user to the userlist ------------------------------------------
    $('#addUserBtn').click(handleCreate);
    $('#usernameInput, #passwordInput, #vornameInput, #nachnameInput, #emailInput, #birthdateInput').keyup(function (event) {
        if (event.which === 13) {
            handleCreate();
        } // only if "enter"-key (=13) is pressed
    });
    //--- 2. click on "save"-button (in edit modal window) or "return": Save detail information of a user --------------
    $('#editSaveBtn').click(handleEditSave);
    $('#editVorname, #editNachname, #editPassword').keyup(function (event) {
        if (event.which === 13) {
            handleEditSave();
        } // only if "enter"-key (=13) is pressed
    });
    //--- 3. click on "cancel"-button (in edit modal window): Hide modal window, without doing anything else -----------
    $('#editCancelBtn').click(handleEditCancel);
    //--- 5. click login button or "cr" on either field  ---------------------------------------------------------------
    $('#loginBtn').click(handleGetLogin);
    $('#username, #password').keyup(function (event) {
        if (event.which === 13) {
            handleGetLogin();
        } // only if "enter"-key (=13) is pressed
    });
    //--- 6. click on the logout button --------------------------------------------------------------------------------
    $('#logoutBtn, #logoutAdminBtn').click(function () {
        handleLogout();
        $("#profilWindow, #profilAdminWindow").hide();
    });
    //--- 8. click on the save button (in settings modal window) -------------------------------------------------------
    //$('#settingsSaveBtn').click(handleSettingsSave);
    //--- 9. click on "cancel"-button (in settings modal window): Hide modal window,, without doing anything else ------
    //$('#settingsCancelBtn').click(handleSettingsCancel);
    //--- 10. click on profile-icon ------------------------------------------------------------------------------------
    $('#profil, #profilSide').click(handleProfile);
    $('#profilAdmin, #profilSideAdmin').click(handleAdminProfile);
    $('#bewertungeinstellenbtn').click(handleBewertungEinstellen);
    $('#bucheinstellenbtn').click(handleBuchEinstellen);
    $(".genreSelect .dropdown-menu li a").click(function (e) {
        e.preventDefault();
        var elem = $(this), select = elem.parents('.genreSelect'), val = elem.data('value');
        select.find('.currentGenreVal').text(val);
        select.find('.selectGenreValue').val(val);
    });
    /**
     * 3. Leaving application
     */
    $(window).on('unload', function () {
        settings.storeSettings();
    }); //--- store settings in local storage
    // Set the date we're counting down to
    var countDownDate = new Date("Feb 05, 2018 20:00:00").getTime();
    // Update the count down every 1 second
    var x = setInterval(function () {
        // Get todays date and time
        var now = new Date().getTime();
        // Find the distance between now an the count down date
        var distance = countDownDate - now;
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        // Output the result in an element with id="demo"
        document.getElementById("demo").innerHTML = hours + "h "
            + minutes + "m " + seconds + "s ";
        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "nicht";
        }
    }, 1000);
});
//# sourceMappingURL=clientCS.js.map