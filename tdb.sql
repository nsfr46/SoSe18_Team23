-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 04. Jul 2018 um 10:35
-- Server-Version: 10.1.32-MariaDB
-- PHP-Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `tdb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bewertung`
--

CREATE TABLE `bewertung` (
  `id` int(11) NOT NULL,
  `bewertungstext` varchar(255) NOT NULL,
  `tausch_id` int(11) NOT NULL,
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `bewertung`
--

INSERT INTO `bewertung` (`id`, `bewertungstext`, `tausch_id`, `userid`) VALUES
(1, 'Sehr schönes Buch!', 1, 1),
(2, 'Schnelle Lieferung!', 2, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `buch`
--

CREATE TABLE `buch` (
  `id` int(11) NOT NULL,
  `Titel` varchar(255) NOT NULL,
  `Autor` varchar(255) NOT NULL,
  `Erscheinungsjahr` date NOT NULL,
  `Verlag` varchar(255) NOT NULL,
  `Sprache` varchar(255) NOT NULL,
  `Seitenzahl` varchar(255) NOT NULL,
  `ISBN` varchar(255) NOT NULL,
  `Beschreibung` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `tausch_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `buch`
--

INSERT INTO `buch` (`id`, `Titel`, `Autor`, `Erscheinungsjahr`, `Verlag`, `Sprache`, `Seitenzahl`, `ISBN`, `Beschreibung`, `user_id`, `tausch_id`, `genre_id`) VALUES
(1, 'Harry Potter und der Stein der Weisen', 'Joanne K. Rowling', '1997-10-11', 'Carlsen', 'Deutsch', '335 Seiten', '978-3551354013', 'Bis zu seinem elften Geburtstag glaubt Harry, er sei ein ganz normaler Junge. Doch dann erfährt er, dass er sich an der Schule für Hexerei und Zauberei einfinden soll – denn er ist ein Zauberer! In Hogwarts stürzt Harry von einem Abenteuer ins nächste und muss gegen Bestien, Mitschüler und Fabelwesen kämpfen. Da ist es gut, dass er schon Freunde gefunden hat, die ihm im Kampf gegen die dunklen Mächte zur Seite stehen.', 1, 1, 6),
(2, 'Harry Potter und die Kammer des Schreckens', 'Joanne K. Rowling\r\n', '2006-06-12', 'Carlsen', 'Deutsch', '368 Seiten', '978-3551354020', 'Endlich wieder Schule! Einen solchen Seufzer kann nur der ausstoßen, dessen Ferien scheußlich waren: Harry Potter. Doch wie im vergangenen Schuljahr stehen nicht nur Zaubertrankunterricht und Verwandlung auf dem Programm. Ein grauenhaftes Etwas treibt sein Unwesen in der Schule. Wird Harry mit Hilfe seiner Freunde Ron und Hermine das Rätsel lösen und Hogwarts von den dunklen Mächten befreien können?', 2, 2, 6);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `genre`
--

CREATE TABLE `genre` (
  `genre_id` int(11) NOT NULL,
  `genre_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `genre`
--

INSERT INTO `genre` (`genre_id`, `genre_name`) VALUES
(1, 'Krimi'),
(2, 'Thriller'),
(3, 'Horror'),
(4, 'Liebesroman'),
(5, 'Historischer Roman'),
(6, 'Fantasy'),
(7, 'Science-Fiction'),
(8, 'Sachbuch');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rights`
--

CREATE TABLE `rights` (
  `id` int(100) UNSIGNED NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

--
-- Daten für Tabelle `rights`
--

INSERT INTO `rights` (`id`, `content`) VALUES
(1, 'User Rights:\r\nEinsicht auf den Mitgliederbereich.\r\nEigene Kommentare und Ratings d?rfen Erstellt, Gelesen, Bearbeitet und Gel?scht werden.'),
(2, 'Admin Rights:\r\nEinsicht auf den Mitgliederbereich.\r\nEinsicht auf eine User-Liste.\r\nErstellen, Auslesen, Bearbeiten und Loeschen von Usern.\r\nEdit-Window fuer Server-/Datenbankoperationen.');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rightsgroup`
--

CREATE TABLE `rightsgroup` (
  `id` int(11) NOT NULL,
  `rightsgroup` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `rightsgroup`
--

INSERT INTO `rightsgroup` (`id`, `rightsgroup`) VALUES
(1, 'User'),
(2, 'Admin'),
(3, 'Portalanbieter');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tausch`
--

CREATE TABLE `tausch` (
  `id` int(11) NOT NULL,
  `buch1_id` int(11) NOT NULL,
  `buch2_id` int(11) NOT NULL,
  `Preis` int(11) NOT NULL,
  `buch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tausch`
--

INSERT INTO `tausch` (`id`, `buch1_id`, `buch2_id`, `Preis`, `buch_id`) VALUES
(1, 1, 2, 0, 1),
(2, 2, 1, 0, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `id` int(100) UNSIGNED NOT NULL,
  `registered` varchar(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `streetname` varchar(50) DEFAULT NULL,
  `housenumber` int(5) DEFAULT NULL,
  `postalcode` int(5) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `emailadress` varchar(50) NOT NULL,
  `rights_id` int(100) UNSIGNED DEFAULT '1',
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `registered`, `firstname`, `lastname`, `username`, `password`, `streetname`, `housenumber`, `postalcode`, `city`, `emailadress`, `rights_id`, `modified`) VALUES
(1, '2018-1-31 ', 'admin', 'admin', 'admin', 'admin', NULL, NULL, NULL, NULL, 'admin', 2, NULL),
(50, '2018-7-3 0', 'niklas', 'tannert', 'niklas', 'niklas', NULL, NULL, NULL, NULL, 'niklas.tannert@gmail.com', 1, NULL),
(53, '2018-7-3 0', 'dennis', 'grillo', 'dennis', 'dennis', NULL, NULL, NULL, NULL, 'dennis@dennis.com', 1, NULL),
(54, '2018-7-3 0', 'nico', 'schaefer', 'nico', 'nico', NULL, NULL, NULL, NULL, 'nico@nico.com', 1, NULL),
(55, '2018-7-3 0', 'maik', 'peters', 'maik', 'maik', NULL, NULL, NULL, NULL, 'maik@maik.com', 1, NULL),
(56, '2018-7-3 1', 'wolfgang', 'fleischhacker', 'wolfgang', 'wolfgang', NULL, NULL, NULL, NULL, 'wolfgang@wolfgang.com', 1, NULL);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `bewertung`
--
ALTER TABLE `bewertung`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tausch_id` (`tausch_id`) USING BTREE,
  ADD KEY `user_id` (`userid`) USING BTREE;

--
-- Indizes für die Tabelle `buch`
--
ALTER TABLE `buch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`) USING BTREE,
  ADD KEY `tausch_id` (`tausch_id`) USING BTREE,
  ADD KEY `genre_id` (`genre_id`) USING BTREE;

--
-- Indizes für die Tabelle `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`genre_id`);

--
-- Indizes für die Tabelle `rights`
--
ALTER TABLE `rights`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `tausch`
--
ALTER TABLE `tausch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buch1_id` (`buch1_id`) USING BTREE,
  ADD KEY `buch2_id` (`buch2_id`) USING BTREE,
  ADD KEY `buch_id` (`buch_id`) USING BTREE;

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rights_id` (`rights_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `rights`
--
ALTER TABLE `rights`
  MODIFY `id` int(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `buch`
--
ALTER TABLE `buch`
  ADD CONSTRAINT `buch_ibfk_1` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`Genre_ID`);

--
-- Constraints der Tabelle `tausch`
--
ALTER TABLE `tausch`
  ADD CONSTRAINT `tausch_ibfk_1` FOREIGN KEY (`Buch1_ID`) REFERENCES `buch` (`id`),
  ADD CONSTRAINT `tausch_ibfk_2` FOREIGN KEY (`Buch2_ID`) REFERENCES `buch` (`id`);

--
-- Constraints der Tabelle `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`rights_id`) REFERENCES `rights` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
