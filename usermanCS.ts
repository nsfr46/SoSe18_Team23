/*****************************************************************************
 ***                                                                         *
 ***  Import some module from node.js (see: expressjs.com/en/4x/api.html)    *
 ***                                                                         *
 *****************************************************************************/
import express    = require ("express");         // routing
import bodyParser = require ("body-parser");     // parsing parameters
import session    = require ('express-session'); // sessions
import db         = require ("mysql");           // mysql database

import {Request, Response, NextFunction} from "express";
import {Connection, MysqlError}          from "mysql";


/*****************************************************************************
 ***                                                                         *
 ***  setup debugger                                                         *
 ***  install with: npm install debug @types/debug                           *
 ***  start in terminal with:     DEBUG=login,http node userman.js (osX)     *
 ***                          set DEBUG=login,http node userman.js (windows) *
 ***  ... or config debug-parameters in "Edit Configurations"                *
 ***  debug even express:         DEBUG=login,http,express:*                 *
 ***                                                                         *
 *****************************************************************************/
import debug = require ("debug");
import {IDebugger} from "debug";
let debugLogin: IDebugger = debug("login");  // Debugging login
let debugHTTP: IDebugger = debug("http");   // debugging http


/*****************************************************************************
 ***                                                                         *
 ***  setup database and its structure                                       *
 ***                                                                         *
 *****************************************************************************/

//---- Object with connection parameters --------------------------------------
let connection: Connection;

/*---- Object with connection parameters --------------------------------------*/
let connection1: Connection = db.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'tdb'
});

connection1.connect(function (err: MysqlError | null) {
    if (!err) {
        connection = connection1;
        console.log("Database is connected successfully...");
        console.log("Go to http://localhost:8080/site/startseite.html");
    }
    else {
        console.log("Error connecting database ...\n" + err);
    }
});

//---- connect to the database ----------------------------------------------------

//--- Data structure that represents a user in database -----------------------
class User {
    id: number;
    time: string; // time-time format defined[RFC 3339] e.g. 2017-12-31T23:59:6
    username: string;
    vorname: string;
    nachname: string;
    password: string;
    email: string;
    rights_id: string;

    constructor(id: number, time: string, username: string, vorname: string, nachname: string,
                password: string, email: string, rights_id: string) {
        this.id = id;
        this.time = time;
        this.username = username;
        this.vorname = vorname;
        this.nachname = nachname;
        this.password = password;
        this.email = email;
        this.rights_id = rights_id;
    }
}

class buch {
    id: number;
    titel: string;
    autor: string;
    genre: string;
    erscheinungsjahr: string;
    verlag: string;
    sprache: string;
    seitenzahl: string;
    isbn: string;
    beschreibung: string;

    constructor(id: number, titel: string, autor: string, genre: string, erscheinungsjahr: string,
                verlag: string, sprache:string, seitenzahl:string, isbn: string, beschreibung: string) {
        this.id = id;
        this.titel = titel;
        this.autor = autor;
        this.genre = genre;
        this.erscheinungsjahr = erscheinungsjahr;
        this.verlag = verlag;
        this.sprache = sprache;
        this.seitenzahl = seitenzahl;
        this.isbn = isbn;
        this.beschreibung =  beschreibung;
    }
}



/*****************************************************************************
 ***                                                                         *
 ***  Create server with admin function and start it                       *
 ***                                                                         *
 *****************************************************************************/
let router = express();
router.listen(8080, "localhost", function () {
    console.log("");
    console.log("-------------------------------------------------------------");
    console.log("  userMan (complete)");
    console.log("  (Dokumentation als API-Doc)");
    console.log("  Dokumentation erstellen mit (im Terminal von webStorm)");
    console.log("     'apidoc -o apidoc -e node_modules' ");
    console.log("  Dokumentation aufrufen:");
    console.log("     Doppelklick auf: apidoc/index.html ");
    console.log("");
    console.log("  Aufruf: http://localhost:8080/site/Startseite.html");
    console.log("-------------------------------------------------------------");
});

/*****************************************************************************
 ***                                                                         *
 *** Function that is called by each route to send data                      *
 *** gets userList from database , constructs and send response              *
 *** status   : HTTP response state            (provided in any case)        *
 *** res      : Response object for responding (provided in any case)        *
 *** message  : Message to be returned         (provided in any case)        *
 *** user     : data of one user     (provided only in one case)             *
 *** username : name of the user     (provided only during login-accesses)   *
 ***                                                                         *
 *****************************************************************************/
function sendData(status: number, req: Request, res: Response, message: string, user: User, username: string) {
    let query: string = 'SELECT id,firstname,lastname,username,password,emailadress FROM user;';
    connection.query(query, function (err: MysqlError | null, rows: any) {
        if (err) { // database error -> set message, rows and status
            message = "Database error: " + err.code;
            rows    = null;
            status  = 505;
        }
        if ( !req.session.username ) { // user not logged in -> dont send userList
            rows = null;
        }
        let query2: string = 'SELECT * from buecher;';
        connection.query(query2, function (err: MysqlError | null, rows2: any) {

        let response : Object = { "message"  : message,
            "buecherList" : rows2,
            "userList" : rows,
            "user"     : user,
            "username" : username,
            };
        res.status(status);  // set HTTP response state, provided as parameter
        res.json(response);  // send HTTP-response
        debugHTTP("--------------------------------------------------------");
        debugHTTP( JSON.stringify(response,null,2) ); // send debug information
    });
        }
    )
}

 /*****************************************************************************
 ***                                                                         *
 *** API descriptions that are used by some api-documentation parts          *
 ***                                                                         *
 *****************************************************************************/
function sendUserData(status: number, req: Request, res: Response, message: string, user: User, username: string) {
    let query: string = 'SELECT id,firstname,lastname,username,password,emailadress,rights_id FROM user;';
    connection.query(query, function (err: MysqlError | null, rows: any) {
        if (err) { // database error -> set message, rows and status
            message = "Database error: " + err.code;
            rows = null;
            status = 505;
        }
        //if ( !req.session.username ) { // user not logged in -> dont send userList
        //rows = null;
        //}
        let response : Object = { "message"  : message,
            "user" : rows,
            "rights_id": user.rights_id
        };

        res.status(status);  // set HTTP response state, provided as parameter
        res.json(response);  // send HTTP-response
        debugHTTP("--------------------------------------------------------");
        debugHTTP(JSON.stringify(response, null, 2)); // send debug information
    });
}

/**
 * --- common api-description: BadRequest ------------------------------
 * @apiDefine BadRequest
 * @apiError (Error 400) {string} message  description of the error
 * @apiError (Error 400) {json[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 400 (Bad Request) Example 1
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "vorname or nachname not provided",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *   ]
 * }
 * @apiErrorExample 400 (Bad Request) Example 2
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "Id 'Hans' not a number",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *   ]
 * }
 */
/**
 * --- common api-description: NotFound --------------------------------
 * @apiDefine NotFound
 * @apiError (Error 404) {string} message  description of the error
 * @apiError (Error 404) {json[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 404 (Not Found) Example
 * HTTP/1.1 404 Not Found
 * {
 *   "message"  : "Id 42 out of index range",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 *
 */
/**
 * --- common api-description: Gone ------------------------------------
 * @apiDefine Gone
 * @apiError (Error 410) {string}   message  description of the error
 * @apiError (Error 410) {object[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 410 (Gone) Example
 * HTTP/1.1 410 Gone
 * {
 *   "message"  : "User with id 2 already deleted",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 */
/**
 * --- common api-description: Forbidden -------------------------------
 * @apiDefine Forbidden
 * @apiError (Error 403) {string}   message  description of the error
 * @apiError (Error 403) {object[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 403 (Forbidden) Example
 * HTTP/1.1 403 Forbidden
 * {
 *   "message"  : "Admin can not be deleted",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 */



/*****************************************************************************
 ***                                                                         *
 ***  Static routers                                                         *
 ***                                                                         *
 ****************************************************************************/
router.use("/site", express.static(__dirname + "/site"));
router.use("/css", express.static(__dirname + "/css"));
router.use("/client", express.static(__dirname + "/client"));
router.use("/jquery", express.static(__dirname + "/jquery"));
router.use("/data", express.static(__dirname + "/data"));

/*****************************************************************************
 ***                                                                         *
 ***  Router for Parsing, Session-Management                                 *
 ***                                                                         *
 *****************************************************************************/
//--- urlencode Data if received ---------------------------------------------
router.use(bodyParser.json());
 //--- session management -----------------------------------------------------
router.use( session( {
  // save session even if not modified
  resave            : true,
  // save session even if not used
  saveUninitialized : true,
  // forces cookie set on every response needed to set expiration (maxAge)
  rolling           : true,
  // name of the cookie set is set by the server
  name              : "mySessionCookie",
  // encrypt session-id in cookie using "secret" as modifier
  secret            : "geheim",
  // set some cookie-attributes. Here expiration-date (offset in ms)
  cookie            : { maxAge: 5 * 60 * 1000 },
} ) );

/*****************************************************************************
 ***                                                                         *
 ***  Routers that are called in any case                                    *
 ***                                                                         *
 *****************************************************************************/
/**
 * --- login with: post /login -----------------------------------------
 * @api        {post} /login loggin user
 * @apiVersion 1.0.0
 * @apiName    Login
 * @apiGroup   login
 * @apiDescription
 * This route checks, if a user is still logged in <br />
 * This is done by checking, if session-variable "username" still exists
 * @apiExample {url} Usage Example
 * http://localhost:8080/login
 */

router.post     ("/login",   function (req: Request, res: Response) {
  let status   : number = 500;  // Initial HTTP response status
  let message  : string   = ""; // To be set
  let username : string   = req.body.username;
  let password : string   = req.body.password;

  //--- user and password provided --------------------------------------------
  if (username != "" && password != "") {
    let getData: [string, string] = [username, password];
    let query: string = 'SELECT * FROM user WHERE username = ? AND password = ?;';
    connection.query(query, getData, function (err: MysqlError|null, rows: any) {
      if (!err) {
        if (rows.length === 1) { // only one dataset must be found -> rows[0]
          message = username + " logged in by username/password";   // message
          debugLogin(req.body.username + " logged in");
          req.session.username = username;
          status = 200;
        } else {
          message = "Not Valid: user '" + username + "' does not match password";
          status = 401;
        }
      } else {
        message = "Database error: " + err.code;
        status = 505;
      }
      sendData(status, req, res, message, null, username);
    });
    //--- either user or password not provided ----------------------------------
  } else {
    message = "Bad Request: not all mandatory parameters provided";
    status = 400;
    sendData(status, req, res, message, null, username);
  }
});

/**
 * --- get user with /user/:username -----------------------------------------
 * @api        {get} /user/:username Read User information
 * @apiVersion 1.0.0
 * @apiName    ReadUser
 * @apiGroup   Use
 *
 * @apiParam {number} :username  URL-parameter: <code>id</code> of the user to be read
 *
 * @apiDescription
 * This route reads the attributes of a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - a user-Object with all attributes of the read user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/hans
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Selected item is Max Mustermann",
 *   "user"     : { {"vorname":"hans",   "nachname":"peter", "time":"23.02.2017  15:27:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */

router.get      ("/user/:username",   function (req: Request, res: Response) {
    let status   : number = 500; // Initial HTTP response status
    let message  : string = "";  // To be set
    let username : string = req.params.username;

    // set up query - parameter is substituted by array-element
    let getData: [string] = [username];
    let query: string = 'SELECT * FROM user WHERE username = ?;';
    connection.query(query, getData, function (err: MysqlError|null, rows: any) {
        let user : User = null;  // initialize user with null
        if (!err) {
            if (rows.length === 1) { // only one dataset must be found -> rows[0]
                user = new User(rows[ 0 ].id as number,rows[ 0 ].time as string,rows[ 0 ].username as string,rows[ 0 ].vorname as string,
                    rows[ 0 ].nachname as string,rows[ 0 ].password as string,rows[ 0 ].email as string,rows[ 0 ].rights_id as string);

                message = "Selected item is " + user.vorname + " " + user.nachname;
                status = 200;
            } else {
                message = "Id " + username + " not found";
                status = 404;
            }
        } else {
            message = "Database error: " + err.code;
            status = 505;
        }
        sendUserData(status, req, res, message, user, null );
    });
});

/**
 * --- create new user with: post /user --------------------------------
 * @api        {post} /user Create new user
 * @apiVersion 1.0.0
 * @apiName    CreateUser
 * @apiGroup   User
 * @apiDescription
 * This route creates a new user with provided parameters and returns <br />
 * - a message with the attributes of the newly created user
 * - a userList containing all users
 * - a user-Object with all attributes of the created user<br/><br/>
 * @apiExample {url} Usage Example
 * http://localhost:8080/user
 *
 * @apiParam {string} vorname  surname of the user
 * @apiParam {string} nachname lastname of the user
 * @apiParamExample {json} Parameters Example
 * vorname=Max&nachname=Mustermann
 *
 * @apiSuccess (Success 201) {string}  message  attributes of newly created user
 * @apiSuccess (Success 201) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 201) {Object}  user that have been created: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 201 (Created) Example
 * HTTP/1.1 201 Created
 * { "message"  : "Sabine Musterfrau successfully added",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017 15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017 15:28:00"} ],
 *   "user"     : { {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017 15:28:00"} } }
 *
 * @apiUse BadRequest
 */
router.post     ("/user", function (req: Request, res: Response) {

    let status      : number = 500; // Initial HTTP response status
    let username    : string = (req.body.username        ? req.body.username:     "").trim();
    let password    : string = (req.body.password        ? req.body.password:     "").trim();
    let vorname     : string = (req.body.vorname         ? req.body.vorname:      "").trim();
    let nachname    : string = (req.body.nachname        ? req.body.nachname:     "").trim();
    let email       : string = (req.body.email           ? req.body.email:        "").trim();
    let message     : string = "";

    //-- ok, if parameters are provided -----------------------------------------
    if ((username != "") && (vorname != "") && (nachname != "") && (email != "") && (password != "")){
        // set up query - parameters are substituted by array-elements
        let insertData: [string, string, string, string, string, string] =
            [ new Date().toLocaleString(), username, password, vorname, nachname, email];
        let query: string = 'INSERT INTO user (registered, username, password, firstname, lastname, emailadress) VALUES (?,?,?,?,?,?);';
        connection.query(query, insertData, function (err: MysqlError | null) {
            if (!err) {
                message = "Created: " + vorname + " " + nachname;
                status = 201;
            } else {
                message = "Database error: " + err.code;
                status = 505;
            }
            sendData(status, req, res, message, null, null);
        });
    }

    //-- nok, if user not provided ----------------------------------------------
    else {
        message = "Bad Request: not all mandatory parameters provided";
        sendData(400, req, res, message, null, null); // send message and all data
    }

});

 /*****************************************************************************
 ***                                                                         *
 ***  Middleware router to check session-id cookie (when already logged in)  *
 ***  Attention:                                                             *
 ***    matches in any case                                                  *
 ***    subsequent routers are only called by "next()" if successfull        *
 ***                                                                         *
 *****************************************************************************/
/*router.use(function (req:Request, res:Response, next:NextFunction) {
  let message  : string = ""; // To be set
  if (req.session.username) { // Check if session still exists
    next();   // call subsequent routers (only if logged in)
  } else {
    message = "Session expired: Please log in again";
    debugLogin("Session expired");
    sendData(401, req, res, message, null, null);
  }
});*/


 /*****************************************************************************
 ***                                                                         *
 ***  Routers that are called only if logged in                              *
 ***                                                                         *
 *****************************************************************************/

/**
 * --- check login with: post /login/check -----------------------------
 * @api        {post} /login/check check if user is logged in
 * @apiVersion 1.0.0
 * @apiName    LoginCheck
 * @apiGroup   login
 * @apiDescription
 * This route checks, if a user is still logged in <br />
 * This is done by checking, if session-variable "username" still exists
 * @apiExample {url} Usage Example
 * http://localhost:8080/login/check
 *
 * @apiSuccess (Success 200) {string}  message  user Musterfrau still logged in
 * @apiSuccess (Success 200) {string}  username name of the user that is logged in
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "user Musterfrau still logged in",
 *   "username" : "Musterfrau"                            }
 */

router.get      ("/login/check", function (req: Request, res: Response) {
  let message : string = "user " + req.session.username + " still logged in";
  sendData(200, req, res, message, null, req.session.username);
});

/**
 * --- logout with: post /logout ---------------------------------------
 * @api        {post} /login/check check if user is logged in
 * @apiVersion 1.0.0
 * @apiName    Logout
 * @apiGroup   login
 * @apiDescription
 * This route logges out a user
 * @apiExample {url} Usage Example
 * http://localhost:8080/logout
 *
 * @apiSuccess (Success 200) {string}  message  user logout successfull
 * @apiSuccess (Success 200) {string}  username name of the user that is logged in
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "Musterfrau logout successfull",
 *   "username" : "Musterfrau"                            }
 */

router.post     ("/logout",     function (req: Request, res: Response) {
  let username : string = req.session.username;
  let message  : string = username + " logout successfull";
  debugLogin(username + " logged out");
  req.session.username = null;
  sendData(200, req, res, message, null, username);
});

/**
 * --- post button with /btn/:id -----------------------------------------
 * @api        {post} /btn/:id Read User information
 * @apiVersion 1.0.0
 * @apiName    ReadClick
 * @apiGroup   Use
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the button to be read
 *
 * @apiDescription
 * This route react on a click and checks the users Session <code>id</code> and returns <br />
 * - a stauts 200 <code>id</code><br />
 * - or drops the session
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/btn/insertbook
 *
 * @apiSuccess (Success 200) {string}  event-handler works as expected <code>id</code>
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Username klicked on button insertbook",
 *   "username"     : "hans" }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router.post     ("/btn/:id",       function (req: Request, res: Response) {
    let id       : string = req.params.id;
    let username : string = req.session.username;
    let message  : string = username + " klicked on button " + id;
    res.status(200);
    res.json( { "message" : message, "username" : username } );

});

/**
 * --- get user with /user/:id -----------------------------------------
 * @api        {get} /user/:id Read User information
 * @apiVersion 1.0.0
 * @apiName    ReadUser
 * @apiGroup   Use
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be read
 *
 * @apiDescription
 * This route reads the attributes of a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - an userList containing all users
 * - a user-Object with all attributes of the read user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/0
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 200) {Object}  user that have been read: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Selected item is Max Mustermann",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router.get      ("/user/:id",   function (req: Request, res: Response) {
    let status   : number = 500; // Initial HTTP response status
    let message  : string = "";  // To be set
    let id       : number = (req.body.id != "" ? req.params.id: -1);

    //-- ok, if id is provided and in valid range and existent ------------------
    if (!isNaN(id) && id >= 0) {
        // set up query - parameter is substituted by array-element
        let getData: [number] = [id];
        let query: string = 'SELECT * FROM user WHERE id = ?;';
        connection.query(query, getData, function (err: MysqlError|null, rows: any) {
            let user : User = null;  // initialize user with null
            if (!err) {
                if (rows.length === 1) { // only one dataset must be found -> rows[0]
                    user = new User(rows[ 0 ].id as number,rows[ 0 ].time as string,rows[ 0 ].username as string,rows[ 0 ].vorname as string,
                        rows[ 0 ].nachname as string,rows[ 0 ].password as string,rows[ 0 ].email as string, rows[ 0 ].rights_id as string);
                    message = "Selected item is " + user.vorname + " " + user.nachname;
                    status = 200;
                } else {
                    message = "Id " + id + " not found";
                    status = 404;
                }
            } else {
                message = "Database error: " + err.code;
                status = 505;
            }
            sendUserData(status, req, res, message, user, null );
        });
    }

    //--- nok, if id not provide, not valid/existent or names not provided ------
    else {
        if (id == -1) {
            message = "Id not provided";
            status = 400;
        } else {
            message = "Id " + id + " not valid";
            status = 500;
        }
        sendUserData(status, req, res, message, null, null);
    }

});

/**
 * --- update user with: put /user/:id ---------------------------------
 * @api        {put} /user/:id Update user
 * @apiVersion 1.0.0
 * @apiName    UpdateUser
 * @apiGroup   User
 * @apiDescription
 * This route changes attributes of a user with provided <code>id</code><br />
 * Only the provided (optional) parameters are hanged.
 * "Update User" returns <br />
 * - a message with the attributes of the updated user
 * - a userList containing all users
 * - a user-Object with all attributes of the updated user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/1
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be updated
 * @apiParam {string} [vorname]  surname of the users
 * @apiParam {string} [nachname] lastname of the user
 *
 * @apiParamExample {urlencoded} Parameters Example
 * vorname=Max&nachname=Mustermann
 *
 * @apiSuccess (Success 200) {string}  message  attributes of newly created user
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 200) {Object}  user the user-data after update: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 201 (Created) Example
 * HTTP/1.1 200 Ok
 * { "message"  : "Updated item is Sabine Mustermann",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Mustermann", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Sabine","nachname":"Mustermann", "time":"23.02.2017  15:28:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */

/* not working!!! */
router.put      ("/user/:id",   function (req: Request, res: Response) {
  let status      : number = 500; // Initial HTTP response status
  let message     : string = ""; // To be set
  let updateData; // No type provided - depends on existence of password
  let query       : string = "";

  //--- check if parameters exists. If not -> initialize ----------------------
  let id       : number = (req.body.id       != "" ? req.params.id     : -1);
  let vorname  : string = (req.body.vorname  != "" ? req.body.vorname  : "");
  let nachname : string = (req.body.nachname != "" ? req.body.nachname : "");
  let password : string = (req.body.password != "" ? req.body.password : "");

  //--- ok, if id is provided, in valid range, existent and names provided ----
  if (!isNaN(id) && id >= 0) {
    // set up query - parameters are substituted by array-elements
    if (password == "") {  // no new password set
      updateData = [vorname, nachname, id];
      query = 'UPDATE user SET vorname = ?, nachname = ? WHERE id = ?;';
    } else {
      updateData = [password, vorname, nachname, id];
      query = 'UPDATE user SET password = ?, vorname = ?, nachname = ? WHERE id = ?;';
    }
    connection.query(query, updateData, function (err: MysqlError|null, rows: any) {
      if (!err) {
        if (rows.affectedRows === 1) {  // only one dataset must be affected
          message = vorname + " "  + nachname  + " successfully updated";
          status = 201;
        } else {
          message = "Not Valid: Id " + id + " not valid";
          status = 500;
        }
      } else {
        message = "Database error: " + err.code;
        status = 505;
      }
      sendData(status, req, res, message, null, null);
    });
  }

  //--- nok, if id not provide, not valid/existent or names not provided ------
  else {
    if (id == -1) {
      message = "Id not provided";
      status = 400;
    } else {
      message = "Bad Request: not all mandatory parameters provided";
      status = 400;
    }
    sendData(status, req, res, message, null, null);
  }

});

/**
 * --- delete user with /user/:id --------------------------------------
 * @api        {delete} /user/:id Delete User
 * @apiVersion 1.0.0
 * @apiName    DeleteUser
 * @apiGroup   User
 *
 * @apiDescription
 * This route deletes a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - a userList containing all users
 * - a user-Object with all attributes of the deleted user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/0
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be deleted
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Max Mustermann has been deleted",
 *   "userList" : [ {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Max","nachname":"Mustermann", "time":"23.02.2017  15:27:00"} }     }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 * @apiUse Forbidden
 */

router.delete   ("/user/:id",   function (req: Request, res: Response) {
  let status    : number = 500; // Initial HTTP response status
  let message   : string = "";  // To be set
  let id        : number = (req.body.id != "" ? req.params.id: -1);

  //-- ok, if id is provided and in valid range and existent ------------------
  if (!isNaN(id) && id > 1) {  // user with id = 1 must not be deleted
    // set up query - parameter is substituted by array-element
    let deleteData: [number] = [id];
    let query: string = 'DELETE FROM user WHERE id = ?;';
    connection.query(query, deleteData, function (err: MysqlError|null, rows: any) {
      if (!err) {
        if (rows.affectedRows > 0) {
          message = "ID " + id + " successfully deleted";
          status = 200;
        } else {
          message = "Id " + id + " not found";
          status = 404;
        }
      } else {
        message = "Database error: " + err.code;
        status = 505;
      }
      sendData(status, req, res, message, null, null);
    });
  }

    router.delete   ("/buecher/:id",   function (req: Request, res: Response) {
        let status    : number = 500; // Initial HTTP response status
        let message   : string = "";  // To be set
        let id        : number = (req.body.id != "" ? req.params.id: -1);

        //-- ok, if id is provided and in valid range and existent ------------------
        if (!isNaN(id) && id > 1) {  // user with id = 1 must not be deleted
            // set up query - parameter is substituted by array-element
            let deleteData: [number] = [id];
            let query: string = 'DELETE FROM buecher WHERE id = ?;';
            connection.query(query, deleteData, function (err: MysqlError|null, rows: any) {
                if (!err) {
                    if (rows.affectedRows > 0) {
                        message = "ID " + id + " successfully deleted";
                        status = 200;
                    } else {
                        message = "Id " + id + " not found";
                        status = 404;
                    }
                } else {
                    message = "Database error: " + err.code;
                    status = 505;
                }
                sendData(status, req, res, message, null, null);
            });
        }
  //-- nok, if id = 1, not provided or not in valid range (not working!!!!) --------------------------
  else {
    if (id == 1 ) {
      message = "Admin can not be deleted";
      status = 403;
    } else if (id == -1) {
      message = "Id not provided";
      status = 400;
    } else {
      message = "Id " + id + " not valid";
      status = 500;
    }
    sendData(status, req, res, message, null, null);
  }

});

/**
 * @api {post} /buch/anlegen Create new Book in Database
 * @apiName CreateBook
 * @apiGroup Book
 *@apiDescription This route creates a new book with provided parameters.
 * @apiParam {titel} titel of book as a string.
 * @apiParam{autor} author of book as a string.
 * @apiParam {erscheinungsjahr} year of book as a string
 * @apiParam {verlag] verlag of a book as a string
 * @apiParam {sprache} speach of a book as a string
 * @apiParam {seitenanzahl} seitenanzahl of a book as a string
 * @apiParam {isbn} ibsn of a book as a string
 * @apiParam {beschreibung} beschreibung of a book as a string
 * @apiExample {url} Usage Example
 * http://localhost:8080/buch/anlegen
 * @apiSuccess (Success 201) {string}  message  attributes of newly created book
 * @apiError {error 505} {string}
 * @apiSuccessExample {json} 201 {Created} Example
 * HTTP/1.1 201 Created
 * { "message"  : "Vielen Dank! Ihr Inserat wird von uns geprüft und in Kürze veröffentlicht.",
 *  "bookList" : [ {"titel":"Harry Potter und der Stein der Weisen",   "autor":"J.K.Rowling",
 *                  "erscheinungsjahr":"2014", "verlag":"Musterverlag", "sprache":"deutsch", "seitenanzahl":"222", "isbn":"56789876789",
 *                  "beschreibung":"Cooles Buch, plaplapla"}
 */
router.post     ("/buch/anlegen", function (req: Request, res: Response) {

    let status      : number = 500; // Initial HTTP response status
    let titel  : string = (req.body.titel  != "" ? req.body.titel  : "");
    let autor : string = (req.body.autor != "" ? req.body.autor : "");
    let erscheinungsjahr : string = (req.body.erscheinungsjahr != "" ? req.body.erscheinungsjahr : "");
    let verlag : string = (req.body.verlag != "" ? req.body.verlag : "");
    let sprache : string = (req.body.sprache != "" ? req.body.sprache : "");
    let seitenanzahl : string = (req.body.seitenanzahl != "" ? req.body.seitenanzahl : "");
    let isbn : string = (req.body.isbn != "" ? req.body.isbn : "");
    let beschreibung : string = (req.body.beschreibung != "" ? req.body.beschreibung : "");

    let message     : string = "";

    //-- ok, if parameters are provided -----------------------------------------
    if ((titel != "") && (autor != "") && (erscheinungsjahr != "") && (verlag != "") && (sprache != "") && (seitenanzahl != "") && (isbn != "") && (beschreibung != "")){
        // set up query - parameters are substituted by array-elements
        let insertData: [string, string, string, string, string, string, string, string] =
            [titel, autor, erscheinungsjahr, verlag, sprache, seitenanzahl, isbn, beschreibung];
        let query: string = 'INSERT INTO buecher (Titel, Autor, Erscheinungsjahr, Verlag, Sprache, Seitenanzahl, ISBN, Beschreibung) VALUES (?,?,?,?,?,?,?,?);';
        connection.query(query, insertData, function (err: MysqlError | null) {
            if (!err) {
                message = "Created: "+ titel;
                status = 201;
            } else {
                message = "Database error: " + err.code;
                status = 505;
            }
            sendData(status, req, res, message, null, null);
        });
    }

    //-- nok, if user not provided ----------------------------------------------
    else {
        message = "Bad Request: not all mandatory parameters provided";
        sendData(400, req, res, message, null, null); // send message and all data
    }

});

router.get    ("/buch/anzeigen", function (req: Request, res: Response) {

    let status      : number = 500; // Initial HTTP response status
    let titel  : string = (req.body.titel  != "" ? req.body.titel  : "");
    let autor : string = (req.body.autor != "" ? req.body.autor : "");
    let erscheinungsjahr : string = (req.body.erscheinungsjahr != "" ? req.body.erscheinungsjahr : "");
    let verlag : string = (req.body.verlag != "" ? req.body.verlag : "");
    let sprache : string = (req.body.sprache != "" ? req.body.sprache : "");
    let seitenanzahl : string = (req.body.seitenanzahl != "" ? req.body.seitenanzahl : "");
    let isbn : string = (req.body.isbn != "" ? req.body.isbn : "");
    let beschreibung : string = (req.body.beschreibung != "" ? req.body.beschreibung : "");

    let message     : string = "";

    //-- ok, if parameters are provided -----------------------------------------
    if ((titel != "") && (autor != "") && (erscheinungsjahr != "") && (verlag != "") && (sprache != "") && (seitenanzahl != "") && (isbn != "") && (beschreibung != "")){
        // set up query - parameters are substituted by array-elements
        let insertData: [string, string, string, string, string, string, string, string] =
            [titel, autor, erscheinungsjahr, verlag, sprache, seitenanzahl, isbn, beschreibung];
        let query: string = 'INSERT INTO buecher (Titel, Autor, Erscheinungsjahr, Verlag, Sprache, Seitenanzahl, ISBN, Beschreibung) VALUES (?,?,?,?,?,?,?,?);';
        connection.query(query, insertData, function (err: MysqlError | null) {
            if (!err) {
                message = "Created: "+ titel;
                status = 201;
            } else {
                message = "Database error: " + err.code;
                status = 505;
            }
            sendData(status, req, res, message, null, null);
        });
    }


    //--- Data structure that represents a rating in database -----------------------


    class bewertung {
        id: number;
        bewertungstext: string;


        constructor(id: number, bewertungstext: string) {
            this.id = id;
            this.bewertungstext = bewertungstext;
        }
    }


    /**
     * @api {post} /bewertung/anlegen Create new Rating in Database
     * @apiName CreateRating
     * @apiGroup Rating
     *@apiDescription This route creates a new Rating with provided parameters.
     * @apiParam {bewertungstext} rating of user as a string.
     * @apiExample {url} Usage Example
     * http://localhost:8080/bewertung/anlegen
     * @apiSuccess (Success 201) {string}  message  attributes of newly created rating
     * @apiError {error 505} {string}
     * @apiSuccessExample {json} 201 {Created} Example
     * HTTP/1.1 201 Created
     * { "message"  : "Vielen Dank! Ihre Bewertung wird von uns geprüft und in Kürze veröffentlicht.",
 */

    router.post     ("/bewertung/anlegen",   function (req: Request, res: Response) {
        let status      : number = 500; // Initial HTTP response status
        let bewertungstext  : string = (req.body.bewertungstext  != "" ? req.body.bewertungstext  : "");

        let message     : string = "";

        //--- ok, if id is provided, in valid range, existent and names provided
        if ((bewertungstext !="")) {
            // set up query - parameters are substituted by array-elements
            let insertData: [string] =
                [bewertungstext];
            let query: string = 'INSERT INTO bewertung (bewertungstext) VALUES (?);';
            connection.query(query, insertData, function (err: MysqlError | null) {
                if (!err) {
                    message = "Created: " + bewertungstext;
                    status = 201;
                } else {
                    message = "Database error: " + err.code;
                    status = 505;
                }
                sendData(status, req, res, message, null, null);
            });
        }

        //-- nok, if user not provided ----------------------------------------------
        else {
            message = "Bad Request: not all mandatory parameters provided";
            sendData(400, req, res, message, null, null); // send message and all data
        }

    });