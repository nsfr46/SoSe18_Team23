"use strict";
exports.__esModule = true;
/*****************************************************************************
 ***                                                                         *
 ***  Import some module from node.js (see: expressjs.com/en/4x/api.html)    *
 ***                                                                         *
 *****************************************************************************/
var express = require("express"); // routing
var bodyParser = require("body-parser"); // parsing parameters
var session = require("express-session"); // sessions
var db = require("mysql"); // mysql database
/*****************************************************************************
 ***                                                                         *
 ***  setup debugger                                                         *
 ***  install with: npm install debug @types/debug                           *
 ***  start in terminal with:     DEBUG=login,http node userman.js (osX)     *
 ***                          set DEBUG=login,http node userman.js (windows) *
 ***  ... or config debug-parameters in "Edit Configurations"                *
 ***  debug even express:         DEBUG=login,http,express:*                 *
 ***                                                                         *
 *****************************************************************************/
var debug = require("debug");
var debugLogin = debug("login"); // Debugging login
var debugHTTP = debug("http"); // debugging http
/*****************************************************************************
 ***                                                                         *
 ***  setup database and its structure                                       *
 ***                                                                         *
 *****************************************************************************/
//---- Object with connection parameters --------------------------------------
var connection;
/*---- Object with connection parameters --------------------------------------*/
var connection1 = db.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'tdb'
});
connection1.connect(function (err) {
    if (!err) {
        connection = connection1;
        console.log("Database is connected successfully...");
        console.log("Go to http://localhost:8080/site/startseite.html");
    }
    else {
        console.log("Error connecting database ...\n" + err);
    }
});
//---- connect to the database ----------------------------------------------------
//--- Data structure that represents a user in database -----------------------
var User = /** @class */ (function () {
    function User(id, time, username, vorname, nachname, password, email, rights_id) {
        this.id = id;
        this.time = time;
        this.username = username;
        this.vorname = vorname;
        this.nachname = nachname;
        this.password = password;
        this.email = email;
        this.rights_id = rights_id;
    }
    return User;
}());
var buch = /** @class */ (function () {
    function buch(id, titel, autor, genre, erscheinungsjahr, verlag, sprache, seitenzahl, isbn, beschreibung) {
        this.id = id;
        this.titel = titel;
        this.autor = autor;
        this.genre = genre;
        this.erscheinungsjahr = erscheinungsjahr;
        this.verlag = verlag;
        this.sprache = sprache;
        this.seitenzahl = seitenzahl;
        this.isbn = isbn;
        this.beschreibung = beschreibung;
    }
    return buch;
}());
/*****************************************************************************
 ***                                                                         *
 ***  Create server with admin function and start it                       *
 ***                                                                         *
 *****************************************************************************/
var router = express();
router.listen(8080, "localhost", function () {
    console.log("");
    console.log("-------------------------------------------------------------");
    console.log("  userMan (complete)");
    console.log("  (Dokumentation als API-Doc)");
    console.log("  Dokumentation erstellen mit (im Terminal von webStorm)");
    console.log("     'apidoc -o apidoc -e node_modules' ");
    console.log("  Dokumentation aufrufen:");
    console.log("     Doppelklick auf: apidoc/index.html ");
    console.log("");
    console.log("  Aufruf: http://localhost:8080/site/Startseite.html");
    console.log("-------------------------------------------------------------");
});
/*****************************************************************************
 ***                                                                         *
 *** Function that is called by each route to send data                      *
 *** gets userList from database , constructs and send response              *
 *** status   : HTTP response state            (provided in any case)        *
 *** res      : Response object for responding (provided in any case)        *
 *** message  : Message to be returned         (provided in any case)        *
 *** user     : data of one user     (provided only in one case)             *
 *** username : name of the user     (provided only during login-accesses)   *
 ***                                                                         *
 *****************************************************************************/
function sendData(status, req, res, message, user, username) {
    var query = 'SELECT id,firstname,lastname,username,password,emailadress FROM user;';
    connection.query(query, function (err, rows) {
        if (err) {
            message = "Database error: " + err.code;
            rows = null;
            status = 505;
        }
        if (!req.session.username) {
            rows = null;
        }
        var query2 = 'SELECT * from buecher;';
        connection.query(query2, function (err, rows2) {
            var response = { "message": message,
                "buecherList": rows2,
                "userList": rows,
                "user": user,
                "username": username
            };
            res.status(status); // set HTTP response state, provided as parameter
            res.json(response); // send HTTP-response
            debugHTTP("--------------------------------------------------------");
            debugHTTP(JSON.stringify(response, null, 2)); // send debug information
        });
    });
}
/*****************************************************************************
***                                                                         *
*** API descriptions that are used by some api-documentation parts          *
***                                                                         *
*****************************************************************************/
function sendUserData(status, req, res, message, user, username) {
    var query = 'SELECT id,firstname,lastname,username,password,emailadress,rights_id FROM user;';
    connection.query(query, function (err, rows) {
        if (err) {
            message = "Database error: " + err.code;
            rows = null;
            status = 505;
        }
        //if ( !req.session.username ) { // user not logged in -> dont send userList
        //rows = null;
        //}
        var response = { "message": message,
            "user": rows,
            "rights_id": user.rights_id
        };
        res.status(status); // set HTTP response state, provided as parameter
        res.json(response); // send HTTP-response
        debugHTTP("--------------------------------------------------------");
        debugHTTP(JSON.stringify(response, null, 2)); // send debug information
    });
}
/**
 * --- common api-description: BadRequest ------------------------------
 * @apiDefine BadRequest
 * @apiError (Error 400) {string} message  description of the error
 * @apiError (Error 400) {json[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 400 (Bad Request) Example 1
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "vorname or nachname not provided",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *   ]
 * }
 * @apiErrorExample 400 (Bad Request) Example 2
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "Id 'Hans' not a number",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *   ]
 * }
 */
/**
 * --- common api-description: NotFound --------------------------------
 * @apiDefine NotFound
 * @apiError (Error 404) {string} message  description of the error
 * @apiError (Error 404) {json[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 404 (Not Found) Example
 * HTTP/1.1 404 Not Found
 * {
 *   "message"  : "Id 42 out of index range",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 *
 */
/**
 * --- common api-description: Gone ------------------------------------
 * @apiDefine Gone
 * @apiError (Error 410) {string}   message  description of the error
 * @apiError (Error 410) {object[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 410 (Gone) Example
 * HTTP/1.1 410 Gone
 * {
 *   "message"  : "User with id 2 already deleted",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 */
/**
 * --- common api-description: Forbidden -------------------------------
 * @apiDefine Forbidden
 * @apiError (Error 403) {string}   message  description of the error
 * @apiError (Error 403) {object[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 403 (Forbidden) Example
 * HTTP/1.1 403 Forbidden
 * {
 *   "message"  : "Admin can not be deleted",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 */
/*****************************************************************************
 ***                                                                         *
 ***  Static routers                                                         *
 ***                                                                         *
 ****************************************************************************/
router.use("/site", express.static(__dirname + "/site"));
router.use("/css", express.static(__dirname + "/css"));
router.use("/client", express.static(__dirname + "/client"));
router.use("/jquery", express.static(__dirname + "/jquery"));
router.use("/data", express.static(__dirname + "/data"));
/*****************************************************************************
 ***                                                                         *
 ***  Router for Parsing, Session-Management                                 *
 ***                                                                         *
 *****************************************************************************/
//--- urlencode Data if received ---------------------------------------------
router.use(bodyParser.json());
//--- session management -----------------------------------------------------
router.use(session({
    // save session even if not modified
    resave: true,
    // save session even if not used
    saveUninitialized: true,
    // forces cookie set on every response needed to set expiration (maxAge)
    rolling: true,
    // name of the cookie set is set by the server
    name: "mySessionCookie",
    // encrypt session-id in cookie using "secret" as modifier
    secret: "geheim",
    // set some cookie-attributes. Here expiration-date (offset in ms)
    cookie: { maxAge: 5 * 60 * 1000 }
}));
/*****************************************************************************
 ***                                                                         *
 ***  Routers that are called in any case                                    *
 ***                                                                         *
 *****************************************************************************/
/**
 * --- login with: post /login -----------------------------------------
 * @api        {post} /login loggin user
 * @apiVersion 1.0.0
 * @apiName    Login
 * @apiGroup   login
 * @apiDescription
 * This route checks, if a user is still logged in <br />
 * This is done by checking, if session-variable "username" still exists
 * @apiExample {url} Usage Example
 * http://localhost:8080/login
 */
router.post("/login", function (req, res) {
    var status = 500; // Initial HTTP response status
    var message = ""; // To be set
    var username = req.body.username;
    var password = req.body.password;
    //--- user and password provided --------------------------------------------
    if (username != "" && password != "") {
        var getData = [username, password];
        var query = 'SELECT * FROM user WHERE username = ? AND password = ?;';
        connection.query(query, getData, function (err, rows) {
            if (!err) {
                if (rows.length === 1) {
                    message = username + " logged in by username/password"; // message
                    debugLogin(req.body.username + " logged in");
                    req.session.username = username;
                    status = 200;
                }
                else {
                    message = "Not Valid: user '" + username + "' does not match password";
                    status = 401;
                }
            }
            else {
                message = "Database error: " + err.code;
                status = 505;
            }
            sendData(status, req, res, message, null, username);
        });
        //--- either user or password not provided ----------------------------------
    }
    else {
        message = "Bad Request: not all mandatory parameters provided";
        status = 400;
        sendData(status, req, res, message, null, username);
    }
});
/**
 * --- get user with /user/:username -----------------------------------------
 * @api        {get} /user/:username Read User information
 * @apiVersion 1.0.0
 * @apiName    ReadUser
 * @apiGroup   Use
 *
 * @apiParam {number} :username  URL-parameter: <code>id</code> of the user to be read
 *
 * @apiDescription
 * This route reads the attributes of a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - a user-Object with all attributes of the read user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/hans
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Selected item is Max Mustermann",
 *   "user"     : { {"vorname":"hans",   "nachname":"peter", "time":"23.02.2017  15:27:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router.get("/user/:username", function (req, res) {
    var status = 500; // Initial HTTP response status
    var message = ""; // To be set
    var username = req.params.username;
    // set up query - parameter is substituted by array-element
    var getData = [username];
    var query = 'SELECT * FROM user WHERE username = ?;';
    connection.query(query, getData, function (err, rows) {
        var user = null; // initialize user with null
        if (!err) {
            if (rows.length === 1) {
                user = new User(rows[0].id, rows[0].time, rows[0].username, rows[0].vorname, rows[0].nachname, rows[0].password, rows[0].email, rows[0].rights_id);
                message = "Selected item is " + user.vorname + " " + user.nachname;
                status = 200;
            }
            else {
                message = "Id " + username + " not found";
                status = 404;
            }
        }
        else {
            message = "Database error: " + err.code;
            status = 505;
        }
        sendUserData(status, req, res, message, user, null);
    });
});
/**
 * --- create new user with: post /user --------------------------------
 * @api        {post} /user Create new user
 * @apiVersion 1.0.0
 * @apiName    CreateUser
 * @apiGroup   User
 * @apiDescription
 * This route creates a new user with provided parameters and returns <br />
 * - a message with the attributes of the newly created user
 * - a userList containing all users
 * - a user-Object with all attributes of the created user<br/><br/>
 * @apiExample {url} Usage Example
 * http://localhost:8080/user
 *
 * @apiParam {string} vorname  surname of the user
 * @apiParam {string} nachname lastname of the user
 * @apiParamExample {json} Parameters Example
 * vorname=Max&nachname=Mustermann
 *
 * @apiSuccess (Success 201) {string}  message  attributes of newly created user
 * @apiSuccess (Success 201) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 201) {Object}  user that have been created: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 201 (Created) Example
 * HTTP/1.1 201 Created
 * { "message"  : "Sabine Musterfrau successfully added",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017 15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017 15:28:00"} ],
 *   "user"     : { {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017 15:28:00"} } }
 *
 * @apiUse BadRequest
 */
router.post("/user", function (req, res) {
    var status = 500; // Initial HTTP response status
    var username = (req.body.username ? req.body.username : "").trim();
    var password = (req.body.password ? req.body.password : "").trim();
    var vorname = (req.body.vorname ? req.body.vorname : "").trim();
    var nachname = (req.body.nachname ? req.body.nachname : "").trim();
    var email = (req.body.email ? req.body.email : "").trim();
    var message = "";
    //-- ok, if parameters are provided -----------------------------------------
    if ((username != "") && (vorname != "") && (nachname != "") && (email != "") && (password != "")) {
        // set up query - parameters are substituted by array-elements
        var insertData = [new Date().toLocaleString(), username, password, vorname, nachname, email];
        var query = 'INSERT INTO user (registered, username, password, firstname, lastname, emailadress) VALUES (?,?,?,?,?,?);';
        connection.query(query, insertData, function (err) {
            if (!err) {
                message = "Created: " + vorname + " " + nachname;
                status = 201;
            }
            else {
                message = "Database error: " + err.code;
                status = 505;
            }
            sendData(status, req, res, message, null, null);
        });
    }
    else {
        message = "Bad Request: not all mandatory parameters provided";
        sendData(400, req, res, message, null, null); // send message and all data
    }
});
/*****************************************************************************
***                                                                         *
***  Middleware router to check session-id cookie (when already logged in)  *
***  Attention:                                                             *
***    matches in any case                                                  *
***    subsequent routers are only called by "next()" if successfull        *
***                                                                         *
*****************************************************************************/
/*router.use(function (req:Request, res:Response, next:NextFunction) {
  let message  : string = ""; // To be set
  if (req.session.username) { // Check if session still exists
    next();   // call subsequent routers (only if logged in)
  } else {
    message = "Session expired: Please log in again";
    debugLogin("Session expired");
    sendData(401, req, res, message, null, null);
  }
});*/
/*****************************************************************************
***                                                                         *
***  Routers that are called only if logged in                              *
***                                                                         *
*****************************************************************************/
/**
 * --- check login with: post /login/check -----------------------------
 * @api        {post} /login/check check if user is logged in
 * @apiVersion 1.0.0
 * @apiName    LoginCheck
 * @apiGroup   login
 * @apiDescription
 * This route checks, if a user is still logged in <br />
 * This is done by checking, if session-variable "username" still exists
 * @apiExample {url} Usage Example
 * http://localhost:8080/login/check
 *
 * @apiSuccess (Success 200) {string}  message  user Musterfrau still logged in
 * @apiSuccess (Success 200) {string}  username name of the user that is logged in
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "user Musterfrau still logged in",
 *   "username" : "Musterfrau"                            }
 */
router.get("/login/check", function (req, res) {
    var message = "user " + req.session.username + " still logged in";
    sendData(200, req, res, message, null, req.session.username);
});
/**
 * --- logout with: post /logout ---------------------------------------
 * @api        {post} /login/check check if user is logged in
 * @apiVersion 1.0.0
 * @apiName    Logout
 * @apiGroup   login
 * @apiDescription
 * This route logges out a user
 * @apiExample {url} Usage Example
 * http://localhost:8080/logout
 *
 * @apiSuccess (Success 200) {string}  message  user logout successfull
 * @apiSuccess (Success 200) {string}  username name of the user that is logged in
 * @apiSuccessExample {json} 200 (OK) Example
 * HTTP/1.1 200 OK
 * { "message"  : "Musterfrau logout successfull",
 *   "username" : "Musterfrau"                            }
 */
router.post("/logout", function (req, res) {
    var username = req.session.username;
    var message = username + " logout successfull";
    debugLogin(username + " logged out");
    req.session.username = null;
    sendData(200, req, res, message, null, username);
});
/**
 * --- post button with /btn/:id -----------------------------------------
 * @api        {post} /btn/:id Read User information
 * @apiVersion 1.0.0
 * @apiName    ReadClick
 * @apiGroup   Use
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the button to be read
 *
 * @apiDescription
 * This route react on a click and checks the users Session <code>id</code> and returns <br />
 * - a stauts 200 <code>id</code><br />
 * - or drops the session
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/btn/insertbook
 *
 * @apiSuccess (Success 200) {string}  event-handler works as expected <code>id</code>
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Username klicked on button insertbook",
 *   "username"     : "hans" }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router.post("/btn/:id", function (req, res) {
    var id = req.params.id;
    var username = req.session.username;
    var message = username + " klicked on button " + id;
    res.status(200);
    res.json({ "message": message, "username": username });
});
/**
 * --- get user with /user/:id -----------------------------------------
 * @api        {get} /user/:id Read User information
 * @apiVersion 1.0.0
 * @apiName    ReadUser
 * @apiGroup   Use
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be read
 *
 * @apiDescription
 * This route reads the attributes of a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - an userList containing all users
 * - a user-Object with all attributes of the read user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/0
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 200) {Object}  user that have been read: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Selected item is Max Mustermann",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router.get("/user/:id", function (req, res) {
    var status = 500; // Initial HTTP response status
    var message = ""; // To be set
    var id = (req.body.id != "" ? req.params.id : -1);
    //-- ok, if id is provided and in valid range and existent ------------------
    if (!isNaN(id) && id >= 0) {
        // set up query - parameter is substituted by array-element
        var getData = [id];
        var query = 'SELECT * FROM user WHERE id = ?;';
        connection.query(query, getData, function (err, rows) {
            var user = null; // initialize user with null
            if (!err) {
                if (rows.length === 1) {
                    user = new User(rows[0].id, rows[0].time, rows[0].username, rows[0].vorname, rows[0].nachname, rows[0].password, rows[0].email, rows[0].rights_id);
                    message = "Selected item is " + user.vorname + " " + user.nachname;
                    status = 200;
                }
                else {
                    message = "Id " + id + " not found";
                    status = 404;
                }
            }
            else {
                message = "Database error: " + err.code;
                status = 505;
            }
            sendUserData(status, req, res, message, user, null);
        });
    }
    else {
        if (id == -1) {
            message = "Id not provided";
            status = 400;
        }
        else {
            message = "Id " + id + " not valid";
            status = 500;
        }
        sendUserData(status, req, res, message, null, null);
    }
});
/**
 * --- update user with: put /user/:id ---------------------------------
 * @api        {put} /user/:id Update user
 * @apiVersion 1.0.0
 * @apiName    UpdateUser
 * @apiGroup   User
 * @apiDescription
 * This route changes attributes of a user with provided <code>id</code><br />
 * Only the provided (optional) parameters are hanged.
 * "Update User" returns <br />
 * - a message with the attributes of the updated user
 * - a userList containing all users
 * - a user-Object with all attributes of the updated user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/1
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be updated
 * @apiParam {string} [vorname]  surname of the users
 * @apiParam {string} [nachname] lastname of the user
 *
 * @apiParamExample {urlencoded} Parameters Example
 * vorname=Max&nachname=Mustermann
 *
 * @apiSuccess (Success 200) {string}  message  attributes of newly created user
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 200) {Object}  user the user-data after update: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 201 (Created) Example
 * HTTP/1.1 200 Ok
 * { "message"  : "Updated item is Sabine Mustermann",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Mustermann", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Sabine","nachname":"Mustermann", "time":"23.02.2017  15:28:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
/* not working!!! */
router.put("/user/:id", function (req, res) {
    var status = 500; // Initial HTTP response status
    var message = ""; // To be set
    var updateData; // No type provided - depends on existence of password
    var query = "";
    //--- check if parameters exists. If not -> initialize ----------------------
    var id = (req.body.id != "" ? req.params.id : -1);
    var vorname = (req.body.vorname != "" ? req.body.vorname : "");
    var nachname = (req.body.nachname != "" ? req.body.nachname : "");
    var password = (req.body.password != "" ? req.body.password : "");
    //--- ok, if id is provided, in valid range, existent and names provided ----
    if (!isNaN(id) && id >= 0) {
        // set up query - parameters are substituted by array-elements
        if (password == "") {
            updateData = [vorname, nachname, id];
            query = 'UPDATE user SET vorname = ?, nachname = ? WHERE id = ?;';
        }
        else {
            updateData = [password, vorname, nachname, id];
            query = 'UPDATE user SET password = ?, vorname = ?, nachname = ? WHERE id = ?;';
        }
        connection.query(query, updateData, function (err, rows) {
            if (!err) {
                if (rows.affectedRows === 1) {
                    message = vorname + " " + nachname + " successfully updated";
                    status = 201;
                }
                else {
                    message = "Not Valid: Id " + id + " not valid";
                    status = 500;
                }
            }
            else {
                message = "Database error: " + err.code;
                status = 505;
            }
            sendData(status, req, res, message, null, null);
        });
    }
    else {
        if (id == -1) {
            message = "Id not provided";
            status = 400;
        }
        else {
            message = "Bad Request: not all mandatory parameters provided";
            status = 400;
        }
        sendData(status, req, res, message, null, null);
    }
});
/**
 * --- delete user with /user/:id --------------------------------------
 * @api        {delete} /user/:id Delete User
 * @apiVersion 1.0.0
 * @apiName    DeleteUser
 * @apiGroup   User
 *
 * @apiDescription
 * This route deletes a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - a userList containing all users
 * - a user-Object with all attributes of the deleted user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/0
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be deleted
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Max Mustermann has been deleted",
 *   "userList" : [ {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Max","nachname":"Mustermann", "time":"23.02.2017  15:27:00"} }     }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 * @apiUse Forbidden
 */
router["delete"]("/user/:id", function (req, res) {
    var status = 500; // Initial HTTP response status
    var message = ""; // To be set
    var id = (req.body.id != "" ? req.params.id : -1);
    //-- ok, if id is provided and in valid range and existent ------------------
    if (!isNaN(id) && id > 1) {
        // set up query - parameter is substituted by array-element
        var deleteData = [id];
        var query = 'DELETE FROM user WHERE id = ?;';
        connection.query(query, deleteData, function (err, rows) {
            if (!err) {
                if (rows.affectedRows > 0) {
                    message = "ID " + id + " successfully deleted";
                    status = 200;
                }
                else {
                    message = "Id " + id + " not found";
                    status = 404;
                }
            }
            else {
                message = "Database error: " + err.code;
                status = 505;
            }
            sendData(status, req, res, message, null, null);
        });
    }
    router["delete"]("/buecher/:id", function (req, res) {
        var status = 500; // Initial HTTP response status
        var message = ""; // To be set
        var id = (req.body.id != "" ? req.params.id : -1);
        //-- ok, if id is provided and in valid range and existent ------------------
        if (!isNaN(id) && id > 1) {
            // set up query - parameter is substituted by array-element
            var deleteData = [id];
            var query = 'DELETE FROM buecher WHERE id = ?;';
            connection.query(query, deleteData, function (err, rows) {
                if (!err) {
                    if (rows.affectedRows > 0) {
                        message = "ID " + id + " successfully deleted";
                        status = 200;
                    }
                    else {
                        message = "Id " + id + " not found";
                        status = 404;
                    }
                }
                else {
                    message = "Database error: " + err.code;
                    status = 505;
                }
                sendData(status, req, res, message, null, null);
            });
        }
        else {
            if (id == 1) {
                message = "Admin can not be deleted";
                status = 403;
            }
            else if (id == -1) {
                message = "Id not provided";
                status = 400;
            }
            else {
                message = "Id " + id + " not valid";
                status = 500;
            }
            sendData(status, req, res, message, null, null);
        }
    });
    /**
     * @api {post} /buch/anlegen Create new Book in Database
     * @apiName CreateBook
     * @apiGroup Book
     *@apiDescription This route creates a new book with provided parameters.
     * @apiParam {titel} titel of book as a string.
     * @apiParam{autor} author of book as a string.
     * @apiParam {erscheinungsjahr} year of book as a string
     * @apiParam {verlag] verlag of a book as a string
     * @apiParam {sprache} speach of a book as a string
     * @apiParam {seitenanzahl} seitenanzahl of a book as a string
     * @apiParam {isbn} ibsn of a book as a string
     * @apiParam {beschreibung} beschreibung of a book as a string
     * @apiExample {url} Usage Example
     * http://localhost:8080/buch/anlegen
     * @apiSuccess (Success 201) {string}  message  attributes of newly created book
     * @apiError {error 505} {string}
     * @apiSuccessExample {json} 201 {Created} Example
     * HTTP/1.1 201 Created
     * { "message"  : "Vielen Dank! Ihr Inserat wird von uns geprüft und in Kürze veröffentlicht.",
     *  "bookList" : [ {"titel":"Harry Potter und der Stein der Weisen",   "autor":"J.K.Rowling",
     *                  "erscheinungsjahr":"2014", "verlag":"Musterverlag", "sprache":"deutsch", "seitenanzahl":"222", "isbn":"56789876789",
     *                  "beschreibung":"Cooles Buch, plaplapla"}
     */
    router.post("/buch/anlegen", function (req, res) {
        var status = 500; // Initial HTTP response status
        var titel = (req.body.titel != "" ? req.body.titel : "");
        var autor = (req.body.autor != "" ? req.body.autor : "");
        var erscheinungsjahr = (req.body.erscheinungsjahr != "" ? req.body.erscheinungsjahr : "");
        var verlag = (req.body.verlag != "" ? req.body.verlag : "");
        var sprache = (req.body.sprache != "" ? req.body.sprache : "");
        var seitenanzahl = (req.body.seitenanzahl != "" ? req.body.seitenanzahl : "");
        var isbn = (req.body.isbn != "" ? req.body.isbn : "");
        var beschreibung = (req.body.beschreibung != "" ? req.body.beschreibung : "");
        var message = "";
        //-- ok, if parameters are provided -----------------------------------------
        if ((titel != "") && (autor != "") && (erscheinungsjahr != "") && (verlag != "") && (sprache != "") && (seitenanzahl != "") && (isbn != "") && (beschreibung != "")) {
            // set up query - parameters are substituted by array-elements
            var insertData = [titel, autor, erscheinungsjahr, verlag, sprache, seitenanzahl, isbn, beschreibung];
            var query = 'INSERT INTO buecher (Titel, Autor, Erscheinungsjahr, Verlag, Sprache, Seitenanzahl, ISBN, Beschreibung) VALUES (?,?,?,?,?,?,?,?);';
            connection.query(query, insertData, function (err) {
                if (!err) {
                    message = "Created: " + titel;
                    status = 201;
                }
                else {
                    message = "Database error: " + err.code;
                    status = 505;
                }
                sendData(status, req, res, message, null, null);
            });
        }
        else {
            message = "Bad Request: not all mandatory parameters provided";
            sendData(400, req, res, message, null, null); // send message and all data
        }
    });
    router.get("/buch/anzeigen", function (req, res) {
        var status = 500; // Initial HTTP response status
        var titel = (req.body.titel != "" ? req.body.titel : "");
        var autor = (req.body.autor != "" ? req.body.autor : "");
        var erscheinungsjahr = (req.body.erscheinungsjahr != "" ? req.body.erscheinungsjahr : "");
        var verlag = (req.body.verlag != "" ? req.body.verlag : "");
        var sprache = (req.body.sprache != "" ? req.body.sprache : "");
        var seitenanzahl = (req.body.seitenanzahl != "" ? req.body.seitenanzahl : "");
        var isbn = (req.body.isbn != "" ? req.body.isbn : "");
        var beschreibung = (req.body.beschreibung != "" ? req.body.beschreibung : "");
        var message = "";
        //-- ok, if parameters are provided -----------------------------------------
        if ((titel != "") && (autor != "") && (erscheinungsjahr != "") && (verlag != "") && (sprache != "") && (seitenanzahl != "") && (isbn != "") && (beschreibung != "")) {
            // set up query - parameters are substituted by array-elements
            var insertData = [titel, autor, erscheinungsjahr, verlag, sprache, seitenanzahl, isbn, beschreibung];
            var query = 'INSERT INTO buecher (Titel, Autor, Erscheinungsjahr, Verlag, Sprache, Seitenanzahl, ISBN, Beschreibung) VALUES (?,?,?,?,?,?,?,?);';
            connection.query(query, insertData, function (err) {
                if (!err) {
                    message = "Created: " + titel;
                    status = 201;
                }
                else {
                    message = "Database error: " + err.code;
                    status = 505;
                }
                sendData(status, req, res, message, null, null);
            });
        }
        //--- Data structure that represents a rating in database -----------------------
        var bewertung = /** @class */ (function () {
            function bewertung(id, bewertungstext) {
                this.id = id;
                this.bewertungstext = bewertungstext;
            }
            return bewertung;
        }());
        /**
         * @api {post} /bewertung/anlegen Create new Rating in Database
         * @apiName CreateRating
         * @apiGroup Rating
         *@apiDescription This route creates a new Rating with provided parameters.
         * @apiParam {bewertungstext} rating of user as a string.
         * @apiExample {url} Usage Example
         * http://localhost:8080/bewertung/anlegen
         * @apiSuccess (Success 201) {string}  message  attributes of newly created rating
         * @apiError {error 505} {string}
         * @apiSuccessExample {json} 201 {Created} Example
         * HTTP/1.1 201 Created
         * { "message"  : "Vielen Dank! Ihre Bewertung wird von uns geprüft und in Kürze veröffentlicht.",
     */
        router.post("/bewertung/anlegen", function (req, res) {
            var status = 500; // Initial HTTP response status
            var bewertungstext = (req.body.bewertungstext != "" ? req.body.bewertungstext : "");
            var message = "";
            //--- ok, if id is provided, in valid range, existent and names provided
            if ((bewertungstext != "")) {
                // set up query - parameters are substituted by array-elements
                var insertData = [bewertungstext];
                var query = 'INSERT INTO bewertung (bewertungstext) VALUES (?);';
                connection.query(query, insertData, function (err) {
                    if (!err) {
                        message = "Created: " + bewertungstext;
                        status = 201;
                    }
                    else {
                        message = "Database error: " + err.code;
                        status = 505;
                    }
                    sendData(status, req, res, message, null, null);
                });
            }
            else {
                message = "Bad Request: not all mandatory parameters provided";
                sendData(400, req, res, message, null, null); // send message and all data
            }
        });
    });
});
//# sourceMappingURL=usermanCS.js.map